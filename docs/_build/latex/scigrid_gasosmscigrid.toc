\select@language {english}
\contentsline {chapter}{\numberline {1}Introduction}{3}{chapter.1}
\contentsline {section}{\numberline {1.1}Project information}{3}{section.1.1}
\contentsline {section}{\numberline {1.2}Background}{4}{section.1.2}
\contentsline {chapter}{\numberline {2}General Information}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}osmscigrid}{5}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Features}{5}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Installation}{5}{subsection.2.1.2}
\contentsline {subsection}{\numberline {2.1.3}Installation qplot}{6}{subsection.2.1.3}
\contentsline {subsection}{\numberline {2.1.4}Download of WorldBorders}{6}{subsection.2.1.4}
\contentsline {subsection}{\numberline {2.1.5}Bugfixes:}{6}{subsection.2.1.5}
\contentsline {subsection}{\numberline {2.1.6}License}{7}{subsection.2.1.6}
\contentsline {subsection}{\numberline {2.1.7}Data\sphinxhyphen {}License}{7}{subsection.2.1.7}
\contentsline {subsection}{\numberline {2.1.8}The Team}{7}{subsection.2.1.8}
\contentsline {chapter}{\numberline {3}Usage}{9}{chapter.3}
\contentsline {section}{\numberline {3.1}Usage}{9}{section.3.1}
\contentsline {section}{\numberline {3.2}Example: Creating a dataset for Ukraine}{9}{section.3.2}
\contentsline {section}{\numberline {3.3}Working with the SciGRID network class}{10}{section.3.3}
\contentsline {section}{\numberline {3.4}Extracting OSM meta data}{12}{section.3.4}
\contentsline {section}{\numberline {3.5}Overview OSM pipeline parameters in Europe}{12}{section.3.5}
\contentsline {section}{\numberline {3.6}Country classification}{15}{section.3.6}
\contentsline {section}{\numberline {3.7}Exporting and Importing Data}{15}{section.3.7}
\contentsline {section}{\numberline {3.8}Visualization}{15}{section.3.8}
