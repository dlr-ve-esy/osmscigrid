# Usage

In the following example we use **osmscigrid** 
to create a SciGRID_gas dataset of gas transport pipelines.
The easiest way to run **osmscigrid** is to download the sample.py 
from the [gitlab repo](https://gitlab.com/dlr-ve-esy/osmscigrid) 
and execute it.
It is quite similar to the more detailed description below.




Example: Creating a dataset for Ukraine
=========
First of all you need to import some functions from the qplot library to your project. 
The *quickplot* function from qplot library is important if you want to visualize your data.


```python

>>> import os
>>> from osmscigrid import M_OSM as M_OSM
>>> from osmscigrid import M_CSV as M_CSV
>>> from osmscigrid.geojson_export import export_geojson
>>> from osmscigrid.osm_tags import extract_tags
>>> from qplot.qplot import quickplot

```
Then, the variable `countrycode` has to be set to the official 2-digit country code of Ukraine.
The location of the used pbf-file which you have previously downloaded from 
[Geofabrik](https://download.geofabrik.de) is stored under `PBF_inputfile`.  
The output protocol file `JSON_outputfile` is automatically used to create the 
output folder for all outputs. 
Next, we define the location for the `TM_World_Borders_file` which 
is used for the country classification.

```python

>>> countrycode="UA"
>>> PBF_inputfile='/srv/ES2050/data/openstreetmap/ukraine-latest.osm.pbf'
>>> JSON_outputfile='./Output/'+countrycode+'/ukraine-latest.json'
>>> TM_World_Borders_file='../TM_World_Borders/TM_WORLD_BORDERS-0.3.shp'

```
Finally, you can use the *read* function from M_OSM to create a SciGRID_gas dataset from your pbf-file.

```python
OSM=M_OSM.read(PBF_inputfile,
               JSON_outputfile,
               TM_World_Borders_file=TM_World_Borders_file, 
               countrycode=countrycode,
               length=20, 
               min_length=0)
```
If you have followed the installation manual above, you can leave out 
the option `TM_World_Borders_file`.
Pipelines in OSM consist of a high number of nodes.
We can reduce the number of nodes by defining the parameter `length`, which will not only
later define the distance between to maximal distance between two nodes,
but recursively remove all intermediate notes or create new ones if necessary.
The coordinates of these removed nodes are stored in the `param` 
dictionary under `path_lat`/`path_long`.
Later, the remaining nodes will be used to create short `PipeSegments`.
In this example, the algorithm will reduce the number of nodes to at least one every 20 km.
Further, we could remove very short pipelines by defining a `min_length` in km,
which should be in all cases significantly smaller than `length`. 

All results are stored to a SciGRID_gas network class. In this example,
we have called the class object 'OSM'.



Working with the SciGRID network class 
=========

You can check out the content of the SciGRID network object with the class method *all*. 

```python
>>> OSM.all()
```
You will get the following output as result:

```
--------------------------------------
Source                               
total component type count          0
--------------------------------------
BorderPoints                        0
Compressors                         0
ConnectionPoints                    0
Consumers                           0
EntryPoints                         0
InterConnectionPoints               0
LNGs                                0
Nodes                           10046
PipeLines                         127
PipePoints                        227
PipeSegments                      154
Processes                           0
Productions                         0
Storages                            0
--------------------------------------
Length of PipeLines    [km]      5243
Length of PipeSegments [km]      5241
```

The SciGRID_gas class object consists of components (e.g `Compressors`, `LNGs`, `PipeSegments`). The components with entries depend on the specific SciGRID_gas dataset. 

* `PipeLines` represent the original pipelines in OSM.
* `PipeSegments` are the same objects as `PipeLines`, but segmented (with `length`).
* `PipePoints` are newly created points during the segmentation process and do not exist directly in OSM.
* `Nodes` are all points referenced in `PipeSegements` und `PipeLines`.

In most cases you are only interested in `PipeSegments`, so let's take a look
at the first element in `PipeSegment` with the *all* method.

(Note: You cannot apply this method to `PipeSegments` without referencing an element through an index number)

```python
>>> OSM.PipeSegments[0].all()
```
You will get the following output:

```python
comments: None
country_code: ['UA', 'UA']
id: OSM-579695348_Seg_1
lat: [49.025480299999955, 48.91379569999983]
long: [33.301573899999894, 32.188594900000076]
method: 
     ('length_km', 'SciGRID_gas')
name: OSM-579695348_Seg_1
node_id: ['OSM_169', 'OSM_111']
param: 
     ('length_km', 82.211)
     path_lat:[ 49.025480299999955 + {166 values} +48.91379569999983]
     path_long:[ 33.301573899999894 + {166 values} +32.188594900000076]
source_id: ['OSM']
tags: 
     ('name', 'placeholder')
     ('depth', '1')
     ('layer', '-2')
     ('usage', 'transmission')
     ('diameter', '1400')
     ('location', 'underground')
     ('man_made', 'pipeline')
     ('operator', 'placeholder')
     ('pressure', '75bar')
     ('substance', 'gas')
     ('start_date', '1980')
     ('contact:phone', '+380 5242 32818;(05242) 32818')
uncertainty: 
     ('length_km', 24.6633)
```
(Note: The value for 'name' and 'operator' have been replaced in this documentation with 'placeholder' due to compatibility issues
with the Cyrillic typeset and latexpdf.)


A single `PipeSegment` consists of two points. The ids of these points are referenced in
`node_id`. Their coordinates are given in the lists `lat` and `long`.

Further, there are 4 dictionaries:
* param
* method
* uncertainty
* tags

Additional meta information about `PipeSegments` are stored in the `param` dictionary.
The `method` dictionary stores information about how values in `param` were created/processed and
the `uncertainty` dictionary stores information about the uncertainty of each 
parameter in `param`. The last one, the `tags` dictionary, 
contains the original information from OSM for each element of a component.

Extracting OSM meta data
=========
Tags are are key-value pairs, which can hold important information on pipelines
such as pressure, diameter, construction date (start_date) and operator. 

We can extracted this information with the *extract_tags* function. 
```python
>>> OSM=extract_tags(OSM)
```
Currently the following information will be extracted from the tags dictionary.: 

* pressure
* diameter 
* start_date 
* operator
 
The information will also be converted to the correct units, if necessary (e.g. [mm], [bar]).
You will find the additional attributes in the `param` dictionary: 

```python
param: 
     ('diameter_mm', 1400)
     ('max_pressure_bar', 75.0)
     ('operator', 'placeholder')
     ('start_date', 1980.0)
```

Other information about gas pipelines are currently not commonly givrn to make them worth extracting. If this changes in the future
the *extract_tags* function could be adapted for additional parameters.

Overview OSM pipeline parameters in Europe
============

We visualize the parameters mentioned above for Europe with the qplot library.
This will give you a better understanding of the current information present in OSM:

![alt text](./images/OSM_diameter.png )

*European gas transport network with diameter values. (OSM: Jan.2021)*



![alt text](./images/OSM_max_pressure.png)

*European gas transport network with max pressure values. (OSM: Jan.2021)*



![alt text](./images/OSM_operator_name.png)

*European gas transport network with existing operator names. (OSM: Jan.2021)*


![alt text](./images/OSM_startdate_exists.png)

*European gas transport network with starting dates. (OSM, Jan. 2021)*

Country classification
=========

Is there a simple method to create a dataset for a specific country from my European data?
Yes! You can create a dataset for whole Europe with the option `countrycode`='EU' 
and the corresponding European pbf file. *osmscigrid* will automatically classify the respectiv countries of each object.
Afterwards you can reduce the dataset to a specific country:
with the class method *reduce*.

```python
countrycode='DE'
OSM_DE=OSM.reduce(countrycode)
```


Exporting and Importing Data
=========

At any time you can save the content of a SciGRID_gas object to CSV or GeoJSON files.

```python 
M_CSV.write(OSM,'OSM',filepath='Output/'+countrycode+'/CSV/')
export_geojson(OSM,"OSM",filepath='Output/'+countrycode+'/GeoJSON/',verbose=False)
```

```python 
export_geojson(OSM,"OSM",filepath='Output/'+countrycode+'/GeoJSON/',verbose=False)
``` 

You can also reload your data, however only from SciGRID_gas CSV-files. 

```python
OSM = M_CSV.read('Output/'+countrycode+'/CSV/',NetzName='OSM',
           NameStart=('OSM'))
```


Visualization
=========

We recommend to visualize SciGRID_gas data with the *quickplot* routine from the
**qplot** library. 


```python
quickplot((OSM,PipeSegments),
          countrycode=countrycode,
          thicklines=True,
          orig_path=True,
          parameter=diameter_mm,
          TM_Borders_filename=TM_World_Borders_file)
```

![alt text](./images/ukraine.png)

*Ukraine gas transport network with diameter values. (OSM: Jan.2021)*

Please check out the qplot documentation for a full description.
However, you can also visualize your data, based on the exported GeoJSON files.
In this case you can use any common GIS application or
drag and drop the files on the following webpage of 
ps://geojson.io. 

![alt text](./images/geojsonio.png)

*Ukraine gas transport network with pipeline diameters.* 





